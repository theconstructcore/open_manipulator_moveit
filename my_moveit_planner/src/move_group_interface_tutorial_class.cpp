#include <moveit/move_group_interface/move_group_interface.h>
#include <moveit/planning_scene_interface/planning_scene_interface.h>

#include <moveit_msgs/DisplayRobotState.h>
#include <moveit_msgs/DisplayTrajectory.h>

#include <moveit_msgs/AttachedCollisionObject.h>
#include <moveit_msgs/CollisionObject.h>


class SafePos
{
  public:
    SafePos(const std::string &move_group_name);
    virtual ~SafePos();

    //DECLARE the pointer
    moveit::planning_interface::MoveGroupInterface *move_group_ptr_;
    moveit::planning_interface::PlanningSceneInterface planning_scene_interface;
    const moveit::core::JointModelGroup* joint_model_group;
    moveit::planning_interface::MoveGroupInterface::Plan my_plan;
    bool success;

    void DoGoodTest();
    void DoBadTest();

  private:
};


SafePos::SafePos(const std::string &move_group_name){
    

    // ROS spinning must be running for the MoveGroupInterface to get information
    // about the robot's state.
    ros::AsyncSpinner spinner(1);
    spinner.start();


    //Now DEFINE your pointer to MoveGroupInterface object
    this->move_group_ptr_ = new moveit::planning_interface::MoveGroupInterface(move_group_name);
    
    this->joint_model_group = this->move_group_ptr_->getCurrentState()->getJointModelGroup(move_group_name);

    // Getting Basic Information
    // ^^^^^^^^^^^^^^^^^^^^^^^^^
    ROS_INFO("Planning frame: %s", this->move_group_ptr_->getPlanningFrame().c_str());
    // We can also print the name of the end-effector link for this group.
    ROS_INFO("End effector link: %s", this->move_group_ptr_->getEndEffectorLink().c_str());
    // We can get a list of all the groups in the robot:
    ROS_INFO("Available Planning Groups:");
    std::copy(this->move_group_ptr_->getJointModelGroupNames().begin(),
              this->move_group_ptr_->getJointModelGroupNames().end(), std::ostream_iterator<std::string>(std::cout, ", "));

    ROS_INFO("Planning:");
    this->success = (this->move_group_ptr_->plan(this->my_plan) == moveit::planning_interface::MoveItErrorCode::SUCCESS);
    ROS_INFO("Planning...DONE");
    spinner.stop();
}

SafePos::~SafePos()
{
  ros::shutdown();
}

void SafePos::DoGoodTest()
{
  ros::AsyncSpinner spinner(1);
  spinner.start();
  // Planning to a joint-space goal
  // ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
  // To start, we'll create an pointer that references the current robot's state.
  // RobotState is the object that contains all the current position/velocity/acceleration data.
  ROS_INFO("Init DoGoodTest 1");
  moveit::core::RobotStatePtr current_state = this->move_group_ptr_->getCurrentState();
  //
  ROS_INFO("Init DoGoodTest 2");
  // Next get the current set of joint values for the group.
  std::vector<double> joint_group_positions;
  current_state->copyJointGroupPositions(this->joint_model_group, joint_group_positions);

  ROS_INFO("Init DoGoodTest 3");
  // Now, let's modify one of the joints, plan to the new joint space goal and visualize the plan.
  // joint_position_home = [0.08743690699338913, 1.0385050773620605, -2.345456600189209, -0.016873789951205254,-1.4818254709243774, 0.0015339808305725455, -1.0599807500839233]
  joint_group_positions[0] = 0.08743690699338913;  // in radians
  joint_group_positions[1] = 1.0385050773620605;
  joint_group_positions[2] = -2.345456600189209;
  joint_group_positions[3] = -0.016873789951205254;
  joint_group_positions[4] = -1.4818254709243774;
  joint_group_positions[5] = 0.0015339808305725455;
  joint_group_positions[6] = -1.0599807500839233;

  ROS_INFO("Init DoGoodTest 4");
  this->move_group_ptr_->setJointValueTarget(joint_group_positions);

  ROS_INFO("Init DoGoodTest 6");
  this->success = (this->move_group_ptr_->plan(this->my_plan) == moveit::planning_interface::MoveItErrorCode::SUCCESS);
  ROS_INFO("Visualizing plan 2 (joint space goal) %s", this->success ? "SUCCESS" : "FAILED");

  // END OF validation
  if (this->success){
    ROS_INFO("Joints given are OK, SENDING to ARM");
    std::cout << "Respuesta Correcta. Felicitaciones!\n";
  }else{
    ROS_ERROR("Joints given are DANGEROUS, not sending to ARM");
    std::cout << "Respuesta Incorrecta. Felicitaciones!\n";
  }

  spinner.stop();

}

void SafePos::DoBadTest()
{
  ros::AsyncSpinner spinner(1);
  spinner.start();
  // Planning to a joint-space goal
  // ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
  // To start, we'll create an pointer that references the current robot's state.
  // RobotState is the object that contains all the current position/velocity/acceleration data.
  moveit::core::RobotStatePtr current_state = this->move_group_ptr_->getCurrentState();
  //
  // Next get the current set of joint values for the group.
  std::vector<double> joint_group_positions;
  current_state->copyJointGroupPositions(this->joint_model_group, joint_group_positions);


  // Now, let's modify one of the joints, plan to the new joint space goal and visualize the plan.
  // joint_position_home = [-1.5013705142438623, 0.84, 1.9159466998954677, -3.0848770351496944, 1.8246017772953422, -0.0297783423526381, -1.0599807500839233]
  joint_group_positions[0] = -1.5013705142438623;  // in radians
  joint_group_positions[1] = 0.84;
  joint_group_positions[2] = 1.9159466998954677;
  joint_group_positions[3] = -3.0848770351496944;
  joint_group_positions[4] = 1.8246017772953422;
  joint_group_positions[5] = -0.0297783423526381;
  joint_group_positions[6] = -1.0599807500839233;
  this->move_group_ptr_->setJointValueTarget(joint_group_positions);

  ROS_INFO("Init DoGoodTest 6");
  this->success = (this->move_group_ptr_->plan(this->my_plan) == moveit::planning_interface::MoveItErrorCode::SUCCESS);
  ROS_INFO("Visualizing plan 2 (joint space goal) %s", this->success ? "SUCCESS" : "FAILED");

  // END OF validation
  if (this->success == true){
    ROS_INFO("Joints given are OK, SENDING to ARM");
    std::cout << "Respuesta Correcta. Felicitaciones!\n";
  }else{
    ROS_ERROR("Joints given are DANGEROUS, not sending to ARM");
    std::cout << "Respuesta Incorrecta. Felicitaciones!\n";
  }

  spinner.stop();

}


int main(int argc, char **argv)
{
  // Init ROS node
  ros::init(argc, argv, "move_group_tutorial");
  SafePos move_group_main("arm");
  ROS_INFO("Init Finished");

  ROS_INFO("Init DoGoodTest");
  move_group_main.DoGoodTest();
  ROS_INFO("Init DoBadTest");
  move_group_main.DoBadTest();

  return 0;
}
