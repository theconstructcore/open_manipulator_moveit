#! /usr/bin/env python

import rospy
from sensor_msgs.msg import JointState
from std_msgs.msg import Header
from my_moveit_planner.srv import JointStatesValidationServiceMessage, JointStatesValidationServiceMessageRequest
import sys
from math import pi

def generate_joint_states_ok():

    joint_goal = JointState()

    h = Header()
    h.stamp = rospy.Time.now()  # Note you need to call rospy.init_node() before this will work
    h.frame_id = ""
    
    joint_pos = [0] * 6
    joint_pos[0] = 0
    joint_pos[1] = -pi/4
    joint_pos[2] = 0
    joint_pos[3] = -pi/2
    joint_pos[4] = 0
    joint_pos[5] = pi/3


    joint_goal.header = h
    joint_goal.position = joint_pos


    return joint_goal

def generate_joint_states_error():

    joint_goal = JointState()

    h = Header()
    h.stamp = rospy.Time.now()  # Note you need to call rospy.init_node() before this will work
    h.frame_id = ""
    
    joint_pos = [0] * 6
    joint_pos[0] = -1.76
    joint_pos[1] = 2.0
    joint_pos[2] = 0
    joint_pos[3] = 0
    joint_pos[4] = 0
    joint_pos[5] = 0

    joint_goal.header = h
    joint_goal.position = joint_pos

    return joint_goal


rospy.init_node('service_client')
rospy.wait_for_service('joint_state_validator')
joint_goal_val_service_client = rospy.ServiceProxy('joint_state_validator', JointStatesValidationServiceMessage)
joint_goal_val_obj = JointStatesValidationServiceMessageRequest()

for i in range(10):
    # OK TEST
    joint_goal_val_obj.joint_states_data = generate_joint_states_ok()
    result = joint_goal_val_service_client(joint_goal_val_obj)
    rospy.logwarn(str(result))
    # DANGEROUS TEST
    joint_goal_val_obj.joint_states_data = generate_joint_states_error()
    result = joint_goal_val_service_client(joint_goal_val_obj)
    rospy.logwarn(str(result))

    rospy.logwarn("Iteration="+str(i))