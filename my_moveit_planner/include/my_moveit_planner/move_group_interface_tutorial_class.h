/*******************************************************************************
* Copyright 2016 ROBOTIS CO., LTD.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*******************************************************************************/

/* Authors: Taehun Lim (Darby) */

#ifndef MY_MOVEIT_PLANNER_MOVEIT_MOVE_GROUP_INTERFACE_TUTORIAL_CLASS_H
#define MY_MOVEIT_PLANNER_MOVEIT_MOVE_GROUP_INTERFACE_TUTORIAL_CLASS_H

#include <moveit/move_group_interface/move_group_interface.h>
#include <moveit/planning_scene_interface/planning_scene_interface.h>

#include <moveit_msgs/DisplayRobotState.h>
#include <moveit_msgs/DisplayTrajectory.h>

#include <moveit_msgs/AttachedCollisionObject.h>
#include <moveit_msgs/CollisionObject.h>

class SafePos
{
 public:
  SafePos();
  virtual ~SafePos();

  moveit::planning_interface::MoveGroupInterface move_group_interface;
  moveit::planning_interface::PlanningSceneInterface planning_scene_interface;
  const moveit::core::JointModelGroup* joint_model_group;
  moveit::planning_interface::MoveGroupInterface::Plan my_plan;
  bool success;

 protected:

   ros::NodeHandle& _nh, _pnh;

 private:

  void initMoveitPlanner();
  void DoGoodTest();
  void DoBadTest();

};

#endif //MY_MOVEIT_PLANNER_MOVEIT_MOVE_GROUP_INTERFACE_TUTORIAL_CLASS_H
